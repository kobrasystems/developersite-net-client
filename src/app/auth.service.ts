import { Injectable } from '@angular/core';
import { Signup } from './signup';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '../environments/environment'
import { AuthResult } from './auth-result';
import { JwtHelperService } from '@auth0/angular-jwt';
import * as moment from 'moment';
import { Login } from './login';


@Injectable({
  providedIn: 'root'
})
export class AuthService {


  constructor(private http: HttpClient, public jwtHelper: JwtHelperService) { }

  private loginUrl = environment.apiUrl + "account/login";
  private signupUrl = environment.apiUrl + 'account/signup';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  signup(signup: Signup): Observable<AuthResult> {
    return this.http.post<AuthResult>(this.signupUrl, signup, this.httpOptions).pipe(
      tap((res: AuthResult) => this.setSession(res))
    )
  }

  login(loginDetails: Login) {
    return this.http.post<AuthResult>(this.loginUrl, loginDetails, this.httpOptions).pipe(
      tap(res => {
        this.setSession(res);
      })
    )
  }

  private setSession(authResult: AuthResult) {
    console.log('setSession has ran');
    const decodedToken = this.jwtHelper.decodeToken(authResult.token);

    if (decodedToken.exp !== undefined) {
      localStorage.setItem('authToken', authResult.token);
    }


  }

  logout() {
    localStorage.removeItem('authToken');
    localStorage.removeItem('expires_at');
  }

  public isAuthenticated(): boolean {
    const authToken: string = localStorage.getItem('authToken') || "";

    return !this.jwtHelper.isTokenExpired(authToken);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);

      return of(result as T);
    }
  }

}