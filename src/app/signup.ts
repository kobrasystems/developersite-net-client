export interface Signup {
    username: string
    password: string
    emailAddress: string
}