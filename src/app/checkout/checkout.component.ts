import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { faCheck, faTimes } from '@fortawesome/free-solid-svg-icons'
import { CheckoutSession } from '../checkout-session';
import { CheckoutService } from '../checkout.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  productId: number = 0;

  faCheck = faCheck;
  faTimes = faTimes;


  constructor(private checkoutService: CheckoutService, private activatedRoute: ActivatedRoute) {
    this.productId = activatedRoute.snapshot.params.id;
  }

  ngOnInit(): void {
    console.log('ngOnInit was fired');
    this.checkoutService.startCheckout(this.productId).subscribe((session: CheckoutSession) => {
      this.checkoutService.redirectToCheckout(session);
    });
  }

}
