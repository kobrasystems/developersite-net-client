import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { CheckoutSession } from './checkout-session';
import { environment } from '../environments/environment';

declare const Stripe: any;

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {
  redirectToCheckout(session: CheckoutSession) {
    const stripe: any = Stripe(session.publicKey);
    stripe.redirectToCheckout({
      sessionId: session.sessionId
    });
  }

  constructor(private http: HttpClient) { }

  checkoutUrl = environment.apiUrl + "payment";

  startCheckout(productId: number): Observable<CheckoutSession> {
    console.log('startCheckout was fired')
    return this.http.post<CheckoutSession>(`${this.checkoutUrl}`, { productId })
      .pipe(
        catchError(this.handleError<CheckoutSession>('startCheckout'))
      )
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);

      return of(result as T);
    }
  }
}
