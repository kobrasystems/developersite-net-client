export interface Product {
    productId: number;
    monthlyCost: number;
    name: string;
    numberOfApplications: number;
    numberOfRequests: number;
    hasControlPanel: boolean;
    hasSupport: boolean;
    productImage: string;
}