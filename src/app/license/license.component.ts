import { Component, OnInit } from '@angular/core';
import { LicenseService } from '../license.service'
import { License } from '../license';
import { faCheck } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-license',
  templateUrl: './license.component.html',
  styleUrls: ['./license.component.css']
})
export class LicenseComponent implements OnInit {
  licenses: License[] = [];
  displayedColumns: string[] = ['licenseType', 'isActive', 'licenseKey']
  faCheck = faCheck

  constructor(private licenseService: LicenseService) { }

  ngOnInit(): void {
    this.getLicenses();
  }

  getLicenses(): void {
    this.licenseService.getLicenses().subscribe(licenses => this.licenses = licenses);
  }

}
