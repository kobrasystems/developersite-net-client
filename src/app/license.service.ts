import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { License } from './license';
import { environment } from '../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class LicenseService {

  private licenseUrl = environment.apiUrl + "license";

  constructor(private http: HttpClient) { }

  getLicenses(): Observable<License[]> {
    return this.http.get<License[]>(this.licenseUrl)
      .pipe(
        catchError(this.handleError<License[]>('getLicenses'))
      )
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);

      return of(result as T);
    }
  }
}
