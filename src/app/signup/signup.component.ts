import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Signup } from '../signup'
import { Router } from '@angular/router'
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  productId: number = 0;

  signup: Signup = {
    username: "",
    password: "",
    emailAddress: ""
  }

  constructor(private signupService: AuthService, private router: Router, private activatedRoute: ActivatedRoute) {
    this.productId = activatedRoute.snapshot.params.id;
  }

  ngOnInit(): void {
  }

  goToCheckout(): void {
    this.router.navigate([`checkout/${this.productId}`]);
  }

  save(): void {
    this.signupService.signup(this.signup).subscribe(() => this.goToCheckout())
  }
}
