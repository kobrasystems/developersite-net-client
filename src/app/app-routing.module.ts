import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductsComponent } from './products/products.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component'
import { AuthGuardService as AuthGuard } from './auth-guard.service';
import { CheckoutComponent } from './checkout/checkout.component';
import { LicenseComponent } from './license/license.component';

const routes: Routes = [
  {
    path: "",
    redirectTo: "/home",
    pathMatch: "full"
  },
  {
    path: "home",
    component: HomeComponent
  },
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "signup/:id",
    component: SignupComponent
  },
  {
    path: "checkout/:id",
    component: CheckoutComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "products",
    component: ProductsComponent
  },
  {
    path: 'licenses',
    component: LicenseComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
