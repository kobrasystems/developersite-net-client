export interface License {
    LicenseTypeId: number
    AccountId: number
    IsActive: number
    LicenseKey: number
    ProductId: number
}